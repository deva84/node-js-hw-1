const express = require('express');
const app = express();
const port = 8080;
const morgan = require('morgan');
const router1 = require('./router');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', router1);

app.use((req, res) => {
    res.status(400).json({message: 'Client error'});
});

app.listen(port, () => {
    console.log(`Server is working on port ${port}...`);
});
