const fs = require('fs').promises;
const regexp = /^.*\.(log|txt|json|yaml|xml|js)$/;

const getFiles = async (req, res) => {
    try {
        const files = await fs.readdir(`${__dirname}/files`);

        res.status(200).json({ message: 'Success', files: files.filter(item => item.match(regexp)) });
    } catch (err) {
        res.status(500).json({ message: 'Server error', err });
    }
}

const getFile = async (req, res) => {
    const current = req.params.filename;

    try {
        const content = await fs.readFile(`./files/${current}`, 'utf-8');
        const uploadedDate = (await fs.stat(`./files/${current}`)).birthtime;

        res.status(200).json({
            message: 'Success',
            filename: current,
            content: content,
            extension: current.substring(current.length, current.lastIndexOf('.') + 1),
            uploadedDate: uploadedDate
        });
    } catch (err) {
        res.status(500).json({ message: 'Server error', err });
    }
}

const createFile = async (req, res) => {
    const { filename, content } = req.body;

    try {
        await fs.writeFile(`./files/${filename}`, content, 'utf-8');
        res.status(200).json({ message: `${filename} has been created`});
    } catch (err) {
        res.status(500).json({ message: 'Server error', err });
    }
}

const changeFile = async (req, res) => {
    const current = req.params.filename;
    const content = req.body.content;

    try {
        await fs.writeFile(`./files/${current}`, content, 'utf-8');
        res.status(200).json({ message: `File content was replaced with the next data: '${content}'` });
    } catch (err) {
        res.status(500).json({ message: 'Server error', err });
    }
}

module.exports = {
    getFiles,
    getFile,
    createFile,
    changeFile
}
