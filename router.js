const express = require('express');
const router = express.Router();

const { valCreation, valReading, valChanging } = require('./validation');
const { getFiles, getFile, createFile, changeFile, } = require('./filemiddlewares');

router.get('/', getFiles);

router.post('/', valCreation, createFile);

router.get('/:filename', valReading, getFile);

router.put('/:filename', valChanging, changeFile);

module.exports = router;

