const fs = require('fs');
const fsPromises = fs.promises;

const regexFile = /\S+\.(log|txt|json|yaml|xml|js)$/i;

const valCreation = async (req, res, next) => {
    const { filename, content } = req.body;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!filename) {
        return res.status(400).json({ message: `Please, specify 'filename' field!` });
    }

    if (!content) {
        return res.status(400).json({ message: `Please, specify 'content' field!` });
    }

    if (!filename.match(regexFile)) {
        return res.status(400).json({ message: `Please, enter valid filename/extension!` });
    }

    if (files.find(item => item === filename)) {
        return res.status(400).json({ message: `${filename} filename already exists!` });
    }

    next();
}

const valReading = async (req, res, next) => {
    const currFile = req.params.filename;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!currFile.match(regexFile)) {
        return res.status(400).json({ message: `Please, enter valid filename/extension!` });
    }

    if (!files.includes(currFile)) {
        return res.status(400).json({ message: `No file with '${currFile}' filename found` });
    }

    next();
}

const valChanging = async (req, res, next) => {
    const content = req.body.content;
    const currFile = req.params.filename;
    const files = await fsPromises.readdir(`${__dirname}/files`);

    if (!content) {
        return res.status(400).json({ message: `Please, specify 'content' field!` });
    }

    if (!currFile.match(regexFile)) {
        return res.status(400).json({ message: `Please, enter valid filename/extension!` });
    }

    if (!files.find(item => item === currFile)) {
        return res.status(400).json({ message: `${currFile} doesn't exists!` });
    }

    next();
}

module.exports = {
    valCreation,
    valReading,
    valChanging
}
